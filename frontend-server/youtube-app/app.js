var React = require('react');
var ReactDOMServer = require('react-dom/server');
var YoutubeApp = require('../../frontend/js/src/components/youtube-app.jsx');

var express = require('express');
var app = express();
app.use(express.static('public'));

app.get('/', function(req, res) {
    console.log('request /');
    res.setHeader('Content-Type', 'text/html');
    const htmlFromReact = ReactDOMServer.renderToString(<YoutubeApp />);

    var outputHtml = "<!DOCTYPE html>" +
            "<html>" +
                "<head>" +
                    "<title>React demo application</title>" +
                    "<link rel='stylesheet' href='/style.css'>" +
                "</head>" +
                "<body class='body'>" +
                    "<div id='youtube-app-container'>" +
                        htmlFromReact +
                    "</div>" +
                "</body>" +
                "<script src='/bundle.js'></script>" +
            "</html>";

    res.send(outputHtml);
});

app.listen(3000, function() {
    console.log('Example app listening on port 3000!');
});