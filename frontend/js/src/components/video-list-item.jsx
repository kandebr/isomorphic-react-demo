"use strict";

var React = require('react');

const VideoListItem = function(props)  {

    const video = props.video;
    const onVideoSelect = props.onVideoSelect;

    const imageUrl = video.snippet.thumbnails.default.url;

    return (
        <li onClick={() => onVideoSelect(video)} className="video-list-item">
            <img
                className="video-list-item__thumb"
                src={imageUrl}
            />
            <div className="video-list-item__name">{video.snippet.title}</div>
        </li>
    );
};

module.exports = VideoListItem;