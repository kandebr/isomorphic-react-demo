"use strict";
var _ = require('lodash');

var React = require('react');
var Component = React.Component;
//var ReactDOM = require('react-dom');
var YTSearch = require('youtube-api-search');

var SearchBar = require('./search-bar.jsx');
var VideoList = require('./video-list.jsx');
var VideoDetails = require('./video-details.jsx');

const API_KEY = "AIzaSyDMyMAp-1bd7HDIaHnhuBnax5zDnAQ5FIA";

var startVideos = [{"kind":"youtube#searchResult","etag":"\"q5k97EMVGxODeKcDgp8gnMu79wM/J3Jv3FppkZBDmOhAJVt_iQLd81g\"","id":{"kind":"youtube#video","videoId":"YSgsLT3ieV8"},"snippet":{"publishedAt":"2016-04-03T13:00:00.000Z","channelId":"UCm9L2p8ow5sxONXELw3-_3A","title":"Feel The Rhythm Inside Austin's Drum Circle: A VR 360° Experience","description":"Austin, TX, is home to some strange traditions. With the all-new Toyota RAV4 Hybrid in tow, hosts Brian Brushwood and Justin Robert Young feel the rhythm and ...","thumbnails":{"default":{"url":"https://i.ytimg.com/vi/YSgsLT3ieV8/default.jpg","width":120,"height":90},"medium":{"url":"https://i.ytimg.com/vi/YSgsLT3ieV8/mqdefault.jpg","width":320,"height":180},"high":{"url":"https://i.ytimg.com/vi/YSgsLT3ieV8/hqdefault.jpg","width":480,"height":360}},"channelTitle":"Seeker Network","liveBroadcastContent":"none"}},{"kind":"youtube#searchResult","etag":"\"q5k97EMVGxODeKcDgp8gnMu79wM/sTCMB_Shz7DDpYAw5d_Vuo07Gl0\"","id":{"kind":"youtube#video","videoId":"hpeF93iQJZE"},"snippet":{"publishedAt":"2016-04-02T13:00:01.000Z","channelId":"UCm9L2p8ow5sxONXELw3-_3A","title":"Cavern Spelunking 150 Feet Underground: A VR 360° Experience","description":"The stars at night are big and bright…but you can't see them when you're 150 feet underground. Brian Brushwood and Justin Robert Young head to Texas's ...","thumbnails":{"default":{"url":"https://i.ytimg.com/vi/hpeF93iQJZE/default.jpg","width":120,"height":90},"medium":{"url":"https://i.ytimg.com/vi/hpeF93iQJZE/mqdefault.jpg","width":320,"height":180},"high":{"url":"https://i.ytimg.com/vi/hpeF93iQJZE/hqdefault.jpg","width":480,"height":360}},"channelTitle":"Seeker Network","liveBroadcastContent":"none"}},{"kind":"youtube#searchResult","etag":"\"q5k97EMVGxODeKcDgp8gnMu79wM/xmtdQ-8rp-nhsBETAfhyawfaXwg\"","id":{"kind":"youtube#video","videoId":"m_QgCPBE1kY"},"snippet":{"publishedAt":"2015-03-14T00:13:44.000Z","channelId":"UCZCDzlWGXZCai8y8Z-2Oi7g","title":"Hamoa Beach VR 360","description":"Hamoa Beach lies at the end of the road to Hana on the Hawaiian Island of Maui. This experience is made for VR, load it up on your mobile device to relax and ...","thumbnails":{"default":{"url":"https://i.ytimg.com/vi/m_QgCPBE1kY/default.jpg","width":120,"height":90},"medium":{"url":"https://i.ytimg.com/vi/m_QgCPBE1kY/mqdefault.jpg","width":320,"height":180},"high":{"url":"https://i.ytimg.com/vi/m_QgCPBE1kY/hqdefault.jpg","width":480,"height":360}},"channelTitle":"360 Labs","liveBroadcastContent":"none"}},{"kind":"youtube#searchResult","etag":"\"q5k97EMVGxODeKcDgp8gnMu79wM/w7tulRDwbtHMlY21wOC4ZbKA8X4\"","id":{"kind":"youtube#video","videoId":"-FyN5_-njAU"},"snippet":{"publishedAt":"2015-08-10T16:59:17.000Z","channelId":"UCpJmBQ8iNHXeQ7jQWDyGe3A","title":"What Happens Inside Your Body? - VR 360°","description":"Take a journey through the human body in Virtual Reality with Life Noggin! HOW TO WATCH: Watch it with maximum quality with the updated youtube app on ...","thumbnails":{"default":{"url":"https://i.ytimg.com/vi/-FyN5_-njAU/default.jpg","width":120,"height":90},"medium":{"url":"https://i.ytimg.com/vi/-FyN5_-njAU/mqdefault.jpg","width":320,"height":180},"high":{"url":"https://i.ytimg.com/vi/-FyN5_-njAU/hqdefault.jpg","width":480,"height":360}},"channelTitle":"Life Noggin","liveBroadcastContent":"none"}},{"kind":"youtube#searchResult","etag":"\"q5k97EMVGxODeKcDgp8gnMu79wM/k_aMPI8nqIeky0InPwyYAzPuDhc\"","id":{"kind":"youtube#video","videoId":"Yp-gMNaWDuE"},"snippet":{"publishedAt":"2016-03-30T07:32:37.000Z","channelId":"UCcyYM25SfSahGD4xnSx3DNw","title":"Welcome to IGN VR! - 360 Degree Video","description":"Subscribe to IGN VR! https://www.youtube.com/c/ignvr?sub_confirmation=1 IGN VR will bring you all the news, reviews, features, 360 degree videos, and pretty ...","thumbnails":{"default":{"url":"https://i.ytimg.com/vi/Yp-gMNaWDuE/default.jpg","width":120,"height":90},"medium":{"url":"https://i.ytimg.com/vi/Yp-gMNaWDuE/mqdefault.jpg","width":320,"height":180},"high":{"url":"https://i.ytimg.com/vi/Yp-gMNaWDuE/hqdefault.jpg","width":480,"height":360}},"channelTitle":"IGN VR","liveBroadcastContent":"none"}}];

class YoutubeApp extends Component {

    constructor(props) {
        super(props);

        this.state = {
            videos: startVideos,
            selectedVideo: startVideos[0]
        };

    }

    videoSearch(term) {
        YTSearch({key: API_KEY, term: term}, videos => {
            this.setState({
                videos: videos,
                selectedVideo: videos[0]
            });
        });
    }

    render() {

        const videoSearch = _.debounce(term => { this.videoSearch(term)}, 300);

        return (
            <div className="youtube-app">
                <div className="youtube-app__header">
                    <SearchBar onSearchTermChange={videoSearch}  />
                </div>
                <div className="youtube-app__content">

                    <div className="youtube-app__video-details">
                        <VideoDetails video={this.state.selectedVideo} />
                    </div>

                    <div className="youtube-app__video-list">
                        <VideoList
                            onVideoSelect={selectedVideo => this.setState({selectedVideo})}
                            videos={this.state.videos}
                        />
                    </div>

                </div>

            </div>
        );
    }
}

module.exports = YoutubeApp;