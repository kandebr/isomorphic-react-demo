"use strict";

var React = require('react');
var ReactDOM = require('react-dom');
var YoutubeApp = require('./components/youtube-app');

// Take this component generated HTML and put it on the page (in the DOM)
ReactDOM.render(<YoutubeApp/>, document.querySelector('#youtube-app-container'));