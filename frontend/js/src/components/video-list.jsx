"use strict";

var React = require('react');
var VideoListItem = require('./video-list-item.jsx');

const VideoList = (props) => {

    const videoItems = props.videos.map((video) => {

        return (
            <VideoListItem
                onVideoSelect={props.onVideoSelect}
                key={video.etag}
                video={video}
            />
        );
    });

    return (
        <ul className="video-list">
            {videoItems}
        </ul>
    );
};

module.exports = VideoList;