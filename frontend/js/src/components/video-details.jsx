var React = require('react');

const VideoDetails = function(props) {

    const video = props.video;

    if (!video) {
        return <div>Loading...</div>;
    }

    const videoId = video.id.videoId;
    const url = `https://www.youtube.com/embed/${videoId}`;

    return (
        <div className="video-details">

            <div className="video-details__frame-wrapper">
                <iframe
                    className="video-details__frame"
                    src={url}>
                </iframe>
            </div>

            <div className="video-details__info">
                <div className="video-details__name">{video.snippet.title}</div>
                <div className="video-details__description">{video.snippet.description}</div>
            </div>
        </div>
    )
};

module.exports = VideoDetails;